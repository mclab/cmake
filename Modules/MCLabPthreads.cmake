macro(mclab_pthreads target)
  find_package(Threads ${ARGN})
  if(THREADS_HAVE_PTHREAD_ARG)
    target_compile_options(PUBLIC ${target} "-pthread")
    endif()
  if(CMAKE_THREAD_LIBS_INIT)
    target_link_libraries(${target} "${CMAKE_THREAD_LIBS_INIT}")
  endif()
endmacro()
