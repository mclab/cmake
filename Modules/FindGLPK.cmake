#.rst:
# FindGLPK
# -------
#

# Finds the GLPK library
#
# This will define the following variables::
#
#   GLPK_FOUND    - True if the system has the GLPK library
#   GLPK_VERSION  - The version of the GLPK library which was found
#
# and the following imported targets::
#
#   GLPK::GLPK   - The GLPK library

find_package(PkgConfig)
pkg_check_modules(PC_GLPK QUIET glib-2.0)

find_path(GLPK_INCLUDE_DIR
  NAMES glpk.h
  PATHS ${PC_GLPK_INCLUDE_DIRS}
)

find_library(GLPK_LIBRARY
  NAMES glpk
  PATHS ${PC_GLPK_LIBRARY_DIRS}
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GLPK
  FOUND_VAR GLPK_FOUND
  REQUIRED_VARS
    GLPK_LIBRARY
    GLPK_INCLUDE_DIR
  VERSION_VAR GLPK_VERSION
)

if(GLPK_FOUND)
  set(GLPK_LIBRARIES ${GLPK_LIBRARY})
  set(GLPK_INCLUDE_DIRS ${GLPK_INCLUDE_DIR})
  set(GLPK_DEFINITIONS ${PC_GLPK_CFLAGS_OTHER})
endif()

if(GLPK_FOUND AND NOT TARGET GLPK::GLPK)
  add_library(GLPK::GLPK UNKNOWN IMPORTED)
  set_target_properties(GLPK::GLPK PROPERTIES
    IMPORTED_LOCATION "${GLPK_LIBRARY}"
    INTERFACE_COMPILE_OPTIONS "${PC_GLPK_CFLAGS_OTHER}"
    INTERFACE_INCLUDE_DIRECTORIES "${GLPK_INCLUDE_DIR}"
  )
endif()

mark_as_advanced(
  GLPK_INCLUDE_DIR
  GLPK_LIBRARY
)
