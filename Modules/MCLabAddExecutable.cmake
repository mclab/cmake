macro(mclab_add_executable exec_name )

add_executable(${exec_name} ${ARGN})

mclab_cflags(${exec_name})

target_include_directories(${exec_name} PUBLIC ${PROJECT_BINARY_DIR})
target_include_directories(${exec_name} PUBLIC ${PROJECT_SOURCE_DIR}/include)
target_include_directories(${exec_name} PUBLIC ${PROJECT_SOURCE_DIR}/src)

endmacro(mclab_add_executable)
