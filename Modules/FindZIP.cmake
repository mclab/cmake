# Searches for an installation of the zip library. On success, it sets the following variables:
#
#   ZIP_FOUND              Set to true to indicate the zip library was found
#   ZIP_INCLUDE_DIR       The directory containing the header file zip/zip.h
#   ZIP_LIBRARIES          The libraries needed to use the zip library
#

find_path(ZIP_INCLUDE_DIR NAMES zip.h)
find_library(ZIP_LIBRARIES NAMES zip)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ZIP DEFAULT_MSG ZIP_INCLUDE_DIR ZIP_LIBRARIES)

mark_as_advanced(ZIP_INCLUDE_DIR ZIP_LIBRARIES)
