macro(mclab_generate_config_header)
  cmake_parse_arguments(MCLAB_GENERATE_CONFIG_HEADER "" "NAME" "" ${ARGN})
  if(NOT MCLAB_GENERATE_CONFIG_HEADER_NAME)
    set(MCLAB_GENERATE_CONFIG_HEADER_NAME "config.h")
  endif()
  configure_file(
    "${PROJECT_SOURCE_DIR}/config.h.in"
    "${PROJECT_BINARY_DIR}/${MCLAB_GENERATE_CONFIG_HEADER_NAME}"
  )
endmacro(mclab_generate_config_header)
